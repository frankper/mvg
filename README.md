Table of Contents
=================

* [Table of Contents](#table-of-contents)
* [VM Template with a collection of dev tools and dotfiles](#wip-vm-template-with-a-collection-of-dev-tool-and-dotfiles)
   * [Prerequisites](#prerequisites)
   * [Versions](#versions)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->

# VM Template with a collection of dev tools and dotfiles
Used for running an isolated dev enviroment based on various operating systems
You can use virtualbox or kvm

The VM uses a bridge interface( you choose the network on first "vagrant up") so that it is accesible by your local network clients and will also be accessible remotely via [tailscale](https://tailscale.com/)

Vagrant is used to provision the VM and you can add your own configuration and credentials into the build process

Switch to each branch for different operating systems
## Prerequisites
* Hypervisor : 

   [KVM](https://linux-kvm.org/page/Main_Page) in Linux

   Testing KVM in wsl2 under windows 11 [using this guide](https://boxofcables.dev/kvm-optimized-custom-kernel-wsl2-2022/)

   Testing [Virtualbox](https://www.virtualbox.org/wiki/Downloads) in Windows 10/11


* Vagrant : 

   [How to install Vagrant](https://www.vagrantup.com/docs/installation)

## Versions
**WORKING**

[Ubuntu](https://gitlab.com/frankper/mvg/-/tree/ubuntu)

[Debian](https://gitlab.com/frankper/mvg/-/tree/debian)

[Arch](https://gitlab.com/frankper/mvg/-/tree/arch)

[Fedora](https://gitlab.com/frankper/mvg/-/tree/fedora)

[Opensuse](https://gitlab.com/frankper/mvg/-/tree/opensuse)

**WIP**

[Windows](https://gitlab.com/frankper/mvg/-/tree/windows)
[Nixos](https://gitlab.com/frankper/mvg/-/tree/nixos)
[Freebsd](https://gitlab.com/frankper/mvg/-/tree/freebsd)
**TBC**

[Mac](https://gitlab.com/frankper/mvg/-/tree/mac)